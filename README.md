# ViPNet-Server

### 1. Introduction
 A VPN Server for Linux and Mini-OS


### 2. Compilation
 To build the server you need the lwip libraries here: http://savannah.nongnu.org/projects/lwip/
 Download them into a directory structure on the same level with ViPNet-Server dir, and you will
 have something like this:

 **2.1 Linux**
```
ViPNet-Server/
Makefile
| -- src
     | -- include
     | -- linux
          | -- Makefile
          | -- lwip_init.c -- Main function
  ..................
  lwip_root_dir
  | -- contrib
  |    | -- apps
  |    | -- ports
  | -- lwip
  |    | -- src
  ..................
```

  After setting the above structure, `cd` into ViPNet-Server and `export` the environment vars:
```sh
  export LWIPDIR=../../<lwip_root_dir>/lwip/src
  export CONTRIBDIR=../../<lwip_root_dir>/contrib
```
  Now you can do `make` and it will build the server.

 **2.2 MiniOS**
  Compilation on MiniOS involves copying Makefile.minios on the minios directoryand type Make -f Makefile.minios
  
  Aditional, the following 2 environment variables must be set:
   - LWIPDIR, with path to lwip main dir (not src, like for Linux)
   - VPNDIR, with path for the main VPN Server directory (not src)
