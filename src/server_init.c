#include "server_init.h"

void server_network_setup(void) {
	struct ip4_addr ip, netmask, gw;

	IP4_ADDR(&ip,      10, 0, 2, 11);
	IP4_ADDR(&netmask, 255, 255, 255, 0);
	IP4_ADDR(&gw,      10, 0, 2, 1);

#if defined (LINUX_VPN)
	netif_set_addr(netif_default, &ip, &netmask, &gw);
#elif defined (MINIOS_VPN)
	start_networking();
	networking_set_addr(&ip, &netmask, &gw);
#endif
}

void print_default_interface(void) {
	struct netif *p;
	struct in_addr inaddr;
	char ip_str[16] = {0}, nm_str[16] = {0}, gw_str[16] = {0};

	p = netif_default;

	inaddr.s_addr = p->ip_addr.addr;
	strncpy(ip_str,inet_ntoa(inaddr),sizeof(ip_str));
	inaddr.s_addr = p->netmask.addr;
	strncpy(nm_str,inet_ntoa(inaddr),sizeof(nm_str));
	inaddr.s_addr = p->gw.addr;
	strncpy(gw_str,inet_ntoa(inaddr),sizeof(gw_str));

	Dprintf("IP: %s, netmask: %s, gateway %s, up: %i\n", ip_str, nm_str, gw_str, netif_is_up(p));
}

void server_init(void *p) {
	LWIP_UNUSED_ARG(p);

	server_network_setup();
	print_default_interface();

	Dprintf("Initial setup finished\n");
	create_thread("udp_server_thread", UDP_server, NULL);
	create_thread("tcp_server_thread", TCP_server, NULL);	
}

int app_main(start_info_t *si) {
	LWIP_UNUSED_ARG(si);
	/* On Mini-OS, the network configuration must be done in a separate thread.
	 * So we create one such thread, that will call the main thread and then will disappear.
	 * On Linux, we also create a new thread and this thread returns to main, where it pauses. See linux_lwip.init.c 
	 */
	create_thread("server_init", server_init, NULL);

	return 0;
}
