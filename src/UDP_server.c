#include "UDP_server.h"

static int find_client_position(void);
static void UDP_sendBroadcast(struct udp_pcb *, char *, u16_t);
static void UDP_sendPacket(struct udp_pcb *, char *, u16_t);
static void UDP_registerConnection(struct udp_pcb *, char *, u16_t, const struct ip4_addr *, u16_t);
static void UDP_recv_handler(void *, struct udp_pcb *, struct pbuf *, const struct ip4_addr *, u16_t);

static struct UDP_client UDP_client[MAX_CLIENTS];
static struct udp_pcb *listener_pcb;
static struct pbuf *write_pbuf;
static char buff_ip[20];

static int find_client_position(void) {
	int i;
	for(i=0; i<MAX_CLIENTS; i++)
		if (UDP_client[i].active == 0)
			return i;
	return -1;
}

static void UDP_sendBroadcast(struct udp_pcb *write_pcb, char *buffer, u16_t size) {
	int i;
	//Dprintf("Broadcasting\n");
	for(i=0; i<MAX_CLIENTS; i++) {
		if (UDP_client[i].active == 0)
			continue;
		//Dprintf("[BROADCAST] Sending packet to %s:%i. Size = %i+%lu\n", copyIpAddressToBuffer(UDP_client[i].ip, 
			// buff_ip), UDP_client[i].port, size, SIZE_PACKET_LEN);
		UDP_WRITE(write_pcb, write_pbuf, buffer, size + SIZE_PACKET_LEN, &UDP_client[i].ip, UDP_client[i].port);
	}
}

static void UDP_sendPacket(struct udp_pcb *write_pcb, char *buffer, u16_t size) {
	int i;
	struct ethhdr *eth = (struct ethhdr *)&buffer[sizeof(size)];

	for(i=0; i<MAX_CLIENTS; i++){
		if (UDP_client[i].active == 0)
			continue;

		if(memcmp(eth->h_dest, UDP_client[i].mac, MAC_SIZE) == 0) {
			//Dprintf("[UNICAST] Sending packet to %s:%i\n", copyIpAddressToBuffer(UDP_client[i].ip, buff_ip),
			//	UDP_client[i].port);
			UDP_WRITE(write_pcb, write_pbuf, buffer, size + SIZE_PACKET_LEN, &UDP_client[i].ip, UDP_client[i].port);
			return;
		}
	}
}

static void UDP_registerConnection(struct udp_pcb *write_pcb, char *buffer, u16_t packetSize, 
	const struct ip4_addr *ip, u16_t port) {
	int index = -1, clientPort = -1;
	struct UDP_client *client = NULL;
	const struct ip4_addr *clientIp = NULL;
	u8_t protocol;

	LWIP_UNUSED_ARG(packetSize);

	/* Use the ip and port from the pcb, instead of the ones from the protocol, because we don't know the protocol */
	if(!UDP_AUTHENTICATION_MESSAGE(buffer, packetSize)) {
		UDP_WRITE(write_pcb, write_pbuf, REJECT_WRONG_AUTH, sizeof(REJECT_WRONG_AUTH), ip, port);
		return;
	}

	protocol = buffer[2 * MAC_SIZE];
	if(protocol == 0x00){
		clientPort = *((u16_t*)&buffer[2*MAC_SIZE + SIZE_PROTOCOL]);
	}
	else if(protocol == 0x01) {
		clientPort = *((u16_t*)&buffer[2*MAC_SIZE + SIZE_PROTOCOL + IP_SIZE]);
	}
	clientIp = ip;

	index = find_client_position();
	if(index == -1){
		UDP_WRITE(write_pcb, write_pbuf, REJECT_NO_SLOTS, sizeof(REJECT_NO_SLOTS), clientIp, clientPort);
		return;
	}

	client = &UDP_client[index];
	client->index = index;
	client->port = clientPort;
	if(protocol == 0x00){
		client->ip = *clientIp;
	}
	else if(protocol == 0x01) {
		memcpy(&client->ip, &buffer[2 * MAC_SIZE + SIZE_PROTOCOL], IP_SIZE);
	}
	memcpy(client->mac, buffer + MAC_SIZE, MAC_SIZE);
	client->active = 1;

	UDP_WRITE(write_pcb, write_pbuf, ACCEPT, sizeof(ACCEPT), &client->ip, client->port);	
	Dprintf("UDP client connected [%i] IP: %s ; MAC: %.2X:%.2X:%.2X:%.2X:%.2X:%.2X ; Port = %i ; Protocol = 0x%.2X\n", 
		client->index,
		copyIpAddressToBuffer(client->ip, buff_ip),
		client->mac[0], client->mac[1], client->mac[2], client->mac[3], client->mac[4], client->mac[5],
		client->port, buffer[2 * MAC_SIZE] );
}


void UDP_recv_handler(void *arg, struct udp_pcb *pcb, struct pbuf *p, const struct ip4_addr *ip, u16_t port) {
	int pBufSize = -1, readSoFar = 0;
	char buffer[BUFSIZE];
	u16_t packetSize = -1;
	struct ethhdr *eth = NULL;
	LWIP_UNUSED_ARG(arg);
	LWIP_UNUSED_ARG(pcb);
	LWIP_UNUSED_ARG(port);

	pBufSize = pBufLen(p);
	while(readSoFar < pBufSize) {
		pbuf_copy_partial(p, buffer, SIZE_PACKET_LEN, readSoFar);
		packetSize = ntohs(*(u16_t*)buffer);

		if(packetSize > pBufSize - readSoFar) {
			Dprintf("Incomplete packet. packetSize=%i readSoFar=%i pBufSize=%i\n", packetSize, readSoFar, pBufSize);
			break;
		}
		
		pbuf_copy_partial(p, &buffer[SIZE_PACKET_LEN], packetSize, readSoFar + SIZE_PACKET_LEN);
		eth = (struct ethhdr *)&buffer[SIZE_PACKET_LEN];

		if(IS_INIT_PACKET(eth))
			UDP_registerConnection(listener_pcb, &buffer[SIZE_PACKET_LEN], packetSize, ip, port);
		else if(IS_BROADCAST_PACKET(eth))
			UDP_sendBroadcast(listener_pcb, buffer, packetSize);
		else
			UDP_sendPacket(listener_pcb, buffer, packetSize);

		readSoFar += packetSize + SIZE_PACKET_LEN;
	}
	pbuf_free(p);
}

void UDP_server(void *args) {
	err_t err;
	LWIP_UNUSED_ARG(args);

	write_pbuf = pbuf_alloc(PBUF_TRANSPORT, BUFSIZE, PBUF_REF);
	LWIP_ASSERT("write_pbuf != NULL", write_pbuf != NULL);
	listener_pcb = udp_new();
	LWIP_ASSERT("listener_pcb != NULL", listener_pcb != NULL);
	err = udp_bind(listener_pcb, IP_ADDR_ANY, UDP_PORT);
	LWIP_ASSERT("err == ERR_OK", err == ERR_OK);
	udp_recv(listener_pcb, UDP_recv_handler, NULL);

	Dprintf("UDP Server is now running.\n");
}
