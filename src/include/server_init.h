#ifndef VPN_SERVER_INIT
#define VPN_SERVER_INIT

/* LINUX */
#if defined (LINUX_VPN)
#include <stdio.h>
#include <linux/if_ether.h>
#include <unistd.h>
#include <string.h>

/* Required when creating the local gateway in linux_lwip_init.c */
#include <lwip/tcpip.h>
#include <netif/tapif.h>
#include <lwip/init.h>

#define create_thread(name, f, args) (sys_thread_new((name), (f), (args), DEFAULT_THREAD_STACKSIZE, \
	DEFAULT_THREAD_PRIO))

typedef void start_info_t;

/* MINI-OS */
#elif defined (MINIOS_VPN)
#include <os.h>
#include <xmalloc.h>
#include <console.h>
#include <netfront.h>

typedef struct ip_addr ip_addr_t;

/* ethhdr - needed in MiniOS */
#define ETH_ALEN    6       /* Octets in one ethernet addr   */
struct ethhdr {
	unsigned char   h_dest[ETH_ALEN];   /* destination eth addr */
	unsigned char   h_source[ETH_ALEN]; /* source ether addr    */
	unsigned short  h_proto;        /* packet type ID field */
} __attribute__((packed));

/* nothing else should work at the moment */
#else
#endif

/* common libraries */
#include <lwip/inet.h>
#include <utils.h>

/* Function definitions */
int app_main(start_info_t *); /* MiniOS calls this when starting the application. We emulate it on Linux too. */
void server_init(void *); /* On a new thread, calls functions below. TCP and UDP server are called on new threads. */
void server_network_setup(void); /* Sets up the application IP, mask and gateway. Overwrites the gateway ! */
void print_default_interface(void); /* A debug information function. */
void TCP_server(void *);
void UDP_server(void *);

#endif
