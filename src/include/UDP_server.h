#ifndef UDP_SERVER_H
#define UDP_SERVER_H

#include <server_init.h>
#include <utils.h>
#include <lwip/udp.h>

struct UDP_client {
	int index;
	unsigned char mac[MAC_SIZE];
	ip_addr_t ip;
	u16_t port;
	int active;
};

/* Constants */
#define BUFSIZE 1514
#define UDP_PORT 55555
#define MAX_CLIENTS 50
#define UDP_INIT_PACKET_SIZE (sizeof(u16_t) + 2 * MAC_SIZE + PORT_SIZE)

#define UDP_WRITE(pcb, pbuf, buffer, size, ip, port)	\
	do {												\
		(pbuf)->payload = (char*)(buffer);				\
		(pbuf)->len = (pbuf)->tot_len = (size);			\
		udp_sendto((pcb), (pbuf), (ip), (port));		\
	} while(0)											\

/* Protocol messages and constants */
#define ACCEPT "ACCEPT"
#define REJECT "REJECT"
#define REJECT_NO_SLOTS (REJECT " - No slots")
#define REJECT_WRONG_AUTH (REJECT " - Wrong authentication message")

#define UDP_AUTHENTICATION_MESSAGE(buffer, packetSize) (	\
	PROT_AUTH_MESSAGE0(buffer, packetSize) ||				\
	PROT_AUTH_MESSAGE1(buffer, packetSize) )

#define PROT_AUTH_MESSAGE0(buffer, packetSize) (	\
	(packetSize) == 15 && (buffer)[12] == 0x00 )

#define PROT_AUTH_MESSAGE1(buffer, packetSize) ( 	\
	(packetSize) == 19 && (buffer)[12] == 0x01 )

#endif

