#ifndef UTILS_H
#define UTILS_H

#include <lwip/ip_addr.h>
#include <server_init.h>

#if defined (MINIOS_VPN)
#define printf printk
#endif

#define TRUE 1
#define FALSE 0

/* Constants */
#define MAC_SIZE 6
#define IP_SIZE 4
#define PORT_SIZE 2
#define SIZE_PACKET_LEN sizeof(u16_t)
#define SIZE_PROTOCOL sizeof(u8_t)

#define IS_BROADCAST_PACKET(packet) (									\
		(packet->h_dest[0] == 0xFF) && (packet->h_dest[1] == 0xFF) && 	\
		(packet->h_dest[2] == 0xFF) && (packet->h_dest[3] == 0xFF) && 	\
		(packet->h_dest[4] == 0xFF) && (packet->h_dest[5] == 0xFF) )	\

#define IS_INIT_PACKET(packet) (									    \
		(packet->h_dest[0] == 0xAA) && (packet->h_dest[1] == 0xBB) && 	\
		(packet->h_dest[2] == 0xCC) && (packet->h_dest[3] == 0xDD) && 	\
		(packet->h_dest[4] == 0xEE) && (packet->h_dest[5] == 0xFF) )	\

#define Dprintf(...)							\
	do {										\
		if(VPN_DEBUG)							\
			printf("[DEBUG]: " __VA_ARGS__);	\
	} while (0)									\

void dumpData(char *, int) __attribute__((unused));
char *copyIpAddressToBuffer(struct ip4_addr, char *);

#define netBufLen(buf) (buf->p->tot_len)
#define pBufLen(p) (p->tot_len)

#endif
