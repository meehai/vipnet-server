#include "TCP_server.h"

static struct TCP_client TCP_clients[MAX_CLIENTS];
static struct tcp_pcb *listener_pcb;

static int find_client_position(void);
static void TCP_closeConnection(struct TCP_client *, struct tcp_pcb *);
static void TCP_sendBroadcast(char *, u16_t);
static void TCP_sendPacket(char *, u16_t);
static void TCP_registerConnection(struct tcp_pcb *, char *, u16_t);
static err_t TCP_recv_handler(void *, struct tcp_pcb *, struct pbuf *, err_t);
static err_t TCP_accept_handler(void *, struct tcp_pcb *, err_t);

static void TCP_closeConnection(struct TCP_client *client, struct tcp_pcb *pcb) {
	if(client) {
		memset(client, 0, sizeof(struct TCP_client));
	}

	tcp_arg(pcb, NULL);
	tcp_sent(pcb, NULL);
	tcp_recv(pcb, NULL);
	tcp_err(pcb, NULL);
	tcp_close(pcb);
}

static int find_client_position(void) {
	/* TODO: Use a list to hold the available slots, so we don't iterate every time. */
	int i;
	for(i=0; i<MAX_CLIENTS; i++)
		if (TCP_clients[i].pcb == NULL)
			return i;
	return -1;
}

static void TCP_sendBroadcast(char *buffer, u16_t size) {
	int i;
	/* Dprintf("Sending broadcast\n"); */
	for(i=0; i<MAX_CLIENTS; i++){
		if (TCP_clients[i].pcb == NULL)
			continue;
		TCP_WRITE(TCP_clients[i].pcb, buffer, size + sizeof(size), TCP_WRITE_FLAG_COPY);
	}
}

static void TCP_sendPacket(char *buffer, u16_t size) {
	int i;
	struct ethhdr *eth = (struct ethhdr *)&buffer[sizeof(size)];

	for(i=0; i<MAX_CLIENTS; i++){
		if (TCP_clients[i].pcb == NULL)
			continue;

		if(memcmp(eth->h_dest, TCP_clients[i].mac, MAC_SIZE) == 0) {
			TCP_WRITE(TCP_clients[i].pcb, buffer, size + sizeof(size), TCP_WRITE_FLAG_COPY);
			return;
		}
	}
}

static void TCP_registerConnection(struct tcp_pcb *pcb, char *buffer, u16_t packetSize) {
	int index = -1;
	struct TCP_client *client = NULL;
	struct ethhdr *eth = NULL;

	eth = (struct ethhdr *)&buffer[SIZE_PACKET_LEN];
	if(!TCP_AUTHENTICATION_MESSAGE(&buffer[SIZE_PACKET_LEN], packetSize)) {
		TCP_WRITE(pcb, REJECT_WRONG_AUTH, sizeof(REJECT_WRONG_AUTH), 0);
		TCP_closeConnection(NULL, pcb);
		return;
	}

	index = find_client_position();
	/* No slots available on the server, send REJECT */
	if(index == -1) {
		TCP_WRITE(pcb, REJECT_NO_SLOTS, sizeof(REJECT_NO_SLOTS), 0);
		TCP_closeConnection(NULL, pcb);
		return;
	}

	/* Otherwise, the client will be indexed in the list */
	client = &TCP_clients[index];
	client->pcb = pcb;
	client->index = index;
	memcpy(&client->ip, &pcb->remote_ip, sizeof(ip_addr_t));
	memcpy(client->mac, eth->h_source, MAC_SIZE);
	tcp_arg(pcb, client);

	/* Send the ACCEPT message */
	TCP_WRITE(pcb, ACCEPT, sizeof(ACCEPT), 0);	
	Dprintf("TCP client connected [%i] IP: %u.%u.%u.%u ; MAC: %.2X:%.2X:%.2X:%.2X:%.2X:%.2X ; Protocol: 0x%.2X\n",
	 	client->index,
		((u8_t *)&client->ip)[0],  ((u8_t *)&client->ip)[1], ((u8_t *)&client->ip)[2], ((u8_t *)&client->ip)[3],
		client->mac[0], client->mac[1], client->mac[2], client->mac[3], client->mac[4], client->mac[5],
		buffer[2 * MAC_SIZE + SIZE_PACKET_LEN] );
}

static err_t TCP_recv_handler(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err) {
	int pBufSize = -1, readSoFar = 0;
	char buffer[BUFSIZE];
	u16_t packetSize = -1;
	struct TCP_client *client = (struct TCP_client *)arg;
	struct ethhdr *eth = NULL;

	if(err != ERR_OK)
		goto out;

	/* Client disconnected, remove him */
	if(!p) {
		TCP_closeConnection(client, pcb);
		return err;
	}

	/* Dprintf("[tcp_recv_handler] Message from recv function. Client=%i, pcb_addr=%p\n", client->index, pcb); */
	pBufSize = pBufLen(p);
	/* Read the packet and see it's type. Keep in mind, multiple packets can come at the same time, se we must
	 * loop until we got all of them based on the length in the header.
	 */
	while(readSoFar < pBufSize) {
		pbuf_copy_partial(p, buffer, SIZE_PACKET_LEN, readSoFar);
		packetSize = ntohs(*(u16_t*)buffer);

		/* Incomplete packet. We don't need to necessarily disconnect the client. */
		if(packetSize > pBufSize - readSoFar) {
			Dprintf("Incomplete packet. packetSize=%i readSoFar=%i pBufSize=%i\n", packetSize, readSoFar, pBufSize);
			goto out;
		}

		pbuf_copy_partial(p, &buffer[SIZE_PACKET_LEN], packetSize, readSoFar + SIZE_PACKET_LEN);
		tcp_recved(pcb, packetSize + SIZE_PACKET_LEN);
		eth = (struct ethhdr *)&buffer[SIZE_PACKET_LEN];
		/* dumpData(buffer, packetSize + SIZE_PACKET_LEN); */
		/*Dprintf("[%i] Received a new packet. pBufSize=%i, packetSize=%i(+%zu), readSoFar=%i\n", client->index,
			pBufSize, packetSize, SIZE_PACKET_LEN, readSoFar); */

		if(IS_INIT_PACKET(eth))
			TCP_registerConnection(pcb, buffer, packetSize);
		else if(IS_BROADCAST_PACKET(eth))
			TCP_sendBroadcast(buffer, packetSize);
		else
			TCP_sendPacket(buffer, packetSize);

		readSoFar += packetSize + SIZE_PACKET_LEN;
	}

out:
	pbuf_free(p);
	return ERR_OK;
}

static err_t TCP_accept_handler(void *arg, struct tcp_pcb *pcb, err_t err) {
	LWIP_UNUSED_ARG(arg);
	if(err != ERR_OK)
		return err;

	tcp_nagle_disable(pcb);
	tcp_recv(pcb, TCP_recv_handler);
	tcp_err(pcb, NULL);
	tcp_accepted((struct tcp_pcb *)arg);
	return ERR_OK;
}

void TCP_server(void *args){
	err_t err;
	LWIP_UNUSED_ARG(args);

	listener_pcb = tcp_new();
	LWIP_ASSERT("listener_pcb != NULL", listener_pcb != NULL);
	err = tcp_bind(listener_pcb, IP_ADDR_ANY, TCP_PORT);
	LWIP_ASSERT("err == ERR_OK", err == ERR_OK);
	listener_pcb = tcp_listen(listener_pcb);
	LWIP_ASSERT("listener_pcb != NULL", listener_pcb != NULL);
	tcp_arg(listener_pcb, listener_pcb);
	tcp_setprio(listener_pcb, TCP_PRIO_MIN);
	tcp_accept(listener_pcb, TCP_accept_handler);

	Dprintf("TCP Server is now running.\n");
}
