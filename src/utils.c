#include <utils.h>

void dumpData(char *buffer, int len) {
	char *p = buffer;
	printf("Data: ");
	while(p - buffer != len) {
		printf("%.02x ", *p & 0xff);
		p++;
	}
	printf("\n");
}

char *copyIpAddressToBuffer(ip_addr_t _addr, char *buffer) {
	u32_t addr = (u32_t)_addr.addr;
	memset(buffer, 0, 16);
	sprintf(buffer, "%u.%u.%u.%u", addr & 0xFF, (addr >> 8) & 0xFF, (addr >> 16) & 0xFF, (addr >> 24) & 0xFF);
	return buffer;
}
