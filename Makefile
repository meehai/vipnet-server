# Linux Makefile
MAKE_DIR = src/linux

all:
	cd $(MAKE_DIR) && $(MAKE) && cd - && mv $(MAKE_DIR)/vpn_server vpn_server

clean:
	cd $(MAKE_DIR) && $(MAKE) clean && cd - && rm vpn_server
